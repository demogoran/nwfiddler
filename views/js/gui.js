var gui = require('nw.gui');
if(!gui.App.initialized)
{
	var opts = [
		/*{
		  key : "F5",
		  active : function() {
			window.location.reload();
		  },
		  failed : function(msg) {
		    console.log(msg);
		  }
		},*/
		/*{
		  key : "Ctrl+Shift+R",
		  active : function() {
			window.location.reload();
		  },
		  failed : function(msg) {
		    console.log(msg);
		  }
		},*/
		{
		  key : "F3",
		  active : function() {
			gui.Window.get().showDevTools();
		  },
		  failed : function(msg) {
		    console.log(msg);
		  }
		}
	];

	opts.forEach(function(option){		
		console.log(option);
		var shortcut = new gui.Shortcut(option);
		gui.App.registerGlobalHotKey(shortcut);
	});


	gui.App.initialized = true;
}

$(document).ready(function(){
	var menu = new gui.Menu();
	gui.Window.get().showDevTools();
	gui.Window.get().hide();
	menu.append(new gui.MenuItem({ label: 'Inspect element', click: function(){
		gui.Window.get().showDevTools();
	}}));

	document.body.addEventListener('contextmenu', function(ev) { 
	  ev.preventDefault();
  	  menu.popup(ev.x, ev.y);
	  return false;
	});
});